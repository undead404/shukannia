import get from 'lodash/get';
import includes from 'lodash/includes';
import isEqual from 'lodash/isEqual';
import map from 'lodash/map';
import reject from 'lodash/reject';
import sortBy from 'lodash/sortBy';
import Link from 'next/link';
import React, { useReducer, useState, useCallback, useMemo } from 'react';
import { Container, Row, Button, Col } from 'reactstrap';
import useCurrentFilterReject from './use-current-filter-reject';
import InclusionMenu from './inclusion-menu';
import options from './options.json';
import styles from './advanced-filter.module.css';
import useNextHref from '../../utils/use-next-href';
import toQueryObject from './to-query-object';

function reduceFilterReject(filterReject, action) {
  const { fieldName, type, value } = action;
  const fieldFilterReject = get(filterReject, fieldName);
  switch (type) {
    case 'filter':
      return {
        ...filterReject,
        [fieldName]: {
          filter: sortBy(value),
          reject: reject(get(fieldFilterReject, 'reject', []), (rejectedItem) =>
            includes(value, rejectedItem),
          ),
        },
      };
    case 'reject':
      return {
        ...filterReject,
        [fieldName]: {
          filter: reject(get(fieldFilterReject, 'filter', []), (filteredItem) =>
            includes(value, filteredItem),
          ),
          reject: sortBy(value),
        },
      };
    default:
      return filterReject;
  }
}

export default function AdvancedFilter() {
  const [isShown, setIsShown] = useState(false);
  const toggle = useCallback(() => setIsShown((value) => !value), []);
  const currentFilterReject = useCurrentFilterReject();
  const [filterState, dispatchFilterState] = useReducer(
    reduceFilterReject,
    currentFilterReject,
  );
  const filterStateQueryObject = useMemo(() => toQueryObject(filterState), [
    filterState,
  ]);
  const href = useNextHref(filterStateQueryObject, '/movies');
  return (
    <Container className={styles.advancedFilter}>
      <Button
        className={styles.toggleButton}
        color="secondary"
        onClick={toggle}
      >
        {isShown ? 'Приховати фільтр' : 'Розгорнути фільтр'}
      </Button>
      {isShown && (
        <Col className="align-items-stretch">
          <Row className={styles.inclusionMenus}>
            {map(options, (optionNames, fieldName) => (
              <InclusionMenu
                key={fieldName}
                dispatchFilterState={dispatchFilterState}
                fieldName={fieldName}
                filterReject={filterState[fieldName]}
                optionNames={optionNames}
              />
            ))}
          </Row>
          <Link href={href}>
            <Button
              className={styles.applyButton}
              color="primary"
              disabled={isEqual(currentFilterReject, filterState)}
              href={href}
              tag="a"
            >
              Застосувати
            </Button>
          </Link>
        </Col>
      )}
    </Container>
  );
}
