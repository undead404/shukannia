import filter from 'lodash/filter';
import get from 'lodash/get';
import includes from 'lodash/includes';
import map from 'lodash/map';
import noop from 'lodash/noop';
import sortBy from 'lodash/sortBy';
import React, { useCallback, useMemo } from 'react';
import Select from 'react-select';
import { Col } from 'reactstrap';
import styles from './inclusion-menu.module.css';
import t from '../../utils/t';

const filterSelectStyles = {
  multiValue: (provided) => ({
    ...provided,
    backgroundColor: '#d4edda',
    color: '#155724',
  }),
};

const rejectSelectStyles = {
  multiValue: (provided) => ({
    ...provided,
    backgroundColor: '#f8d7da',
    color: '#721c24',
  }),
};

export default function InclusionMenu(props) {
  const fieldName = get(props, 'fieldName');
  const dispatchFilterState = get(props, 'dispatchFilterState', noop);
  const filterReject = get(props, 'filterReject', {});
  const optionNames = sortBy(get(props, 'optionNames', []));
  const onFilterChange = useCallback(
    (value) => {
      dispatchFilterState({
        fieldName,
        type: 'filter',
        value: map(value, 'value'),
      });
    },
    [dispatchFilterState, fieldName],
  );
  const onRejectChange = useCallback(
    (value) => {
      dispatchFilterState({
        fieldName,
        type: 'reject',
        value: map(value, 'value'),
      });
    },
    [dispatchFilterState, fieldName],
  );
  const selectOptions = useMemo(
    () =>
      map(optionNames, (key) => ({
        label: t(`${fieldName}.${key}`),
        value: key,
      })),
    [optionNames, fieldName],
  );
  const optionsToFilter = useMemo(
    () =>
      filter(selectOptions, (selectOption) =>
        includes(filterReject.filter, selectOption.value),
      ),
    [filterReject, selectOptions],
  );
  const optionsToReject = useMemo(
    () =>
      filter(selectOptions, (selectOption) =>
        includes(filterReject.reject, selectOption.value),
      ),
    [filterReject, selectOptions],
  );
  return (
    <Col className={styles.inclusionMenu} xs={6}>
      <Col>
        <h4>{t(`filters.${fieldName}`)} до вибору</h4>
        <Select
          clearable
          isMulti
          onChange={onFilterChange}
          options={selectOptions}
          placeholder="Обрати..."
          styles={filterSelectStyles}
          value={optionsToFilter}
        />
      </Col>
      <Col>
        <h4>{t(`filters.${fieldName}`)} до виключення</h4>
        <Select
          clearable
          isMulti
          onChange={onRejectChange}
          options={selectOptions}
          placeholder="Обрати..."
          styles={rejectSelectStyles}
          value={optionsToReject}
        />
      </Col>
    </Col>
  );
}
