import forEach from 'lodash/forEach';
import mapValues from 'lodash/mapValues';
import pick from 'lodash/pick';
import split from 'lodash/split';
import startsWith from 'lodash/startsWith';

export default function toFilterReject(queryObject) {
  return mapValues(
    pick(queryObject, ['country', 'genre', 'kind', 'language']),
    (value) => {
      const toFilter = [];
      const toReject = [];
      forEach(split(value, ','), (item) => {
        if (startsWith(item, '!')) {
          toReject.push(item.slice(1));
        } else {
          toFilter.push(item);
        }
      });
      return { filter: toFilter, reject: toReject };
    },
  );
}
