import difference from 'lodash/difference';
import get from 'lodash/get';
import join from 'lodash/join';
import map from 'lodash/map';
import mapValues from 'lodash/mapValues';
import defaultFilterReject from './default-filter-reject.json';

export default function toQueryObject(filterReject) {
  return {
    country: null,
    genre: null,
    kind: null,
    language: null,
    ...mapValues(filterReject, (fieldFilterReject, fieldName) =>
      join(
        [
          ...difference(
            fieldFilterReject.filter,
            get(defaultFilterReject, `${fieldName}.filter`),
          ),
          ...map(
            difference(
              fieldFilterReject.reject,
              get(defaultFilterReject, `${fieldName}.reject`),
            ),
            (rejectedItem) => `!${rejectedItem}`,
          ),
        ],
        ',',
      ),
    ),
  };
}
