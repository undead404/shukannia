import difference from 'lodash/difference';
import forEach from 'lodash/forEach';
import get from 'lodash/get';
import uniq from 'lodash/uniq';
import { useMemo } from 'react';
import useQueryObject from '../../utils/use-query-object';
import defaultFilterReject from './default-filter-reject.json';
import toFilterReject from './to-filter-reject';

export default function useCurrentFilterReject() {
  const queryObject = useQueryObject();
  const currentFilterReject = useMemo(() => {
    const cfr = toFilterReject(queryObject);
    forEach(defaultFilterReject, (fieldFilterReject, fieldName) => {
      const actualToFilter = get(cfr, `${fieldName}.filter`, []);
      const defaultToFilter = get(
        defaultFilterReject,
        `${fieldName}.filter`,
        [],
      );
      const actualToReject = get(cfr, `${fieldName}.reject`, []);
      const defaultToReject = get(
        defaultFilterReject,
        `${fieldName}.reject`,
        [],
      );
      cfr[fieldName] = {
        filter: uniq([
          ...actualToFilter,
          ...difference(defaultToFilter, actualToReject),
        ]),
        reject: uniq([
          ...actualToReject,
          ...difference(defaultToReject, actualToFilter),
        ]),
      };
    });
    return cfr;
  }, [queryObject]);
  return currentFilterReject;
}
