import countryCodeLookup from 'country-code-lookup';
import Flags from 'country-flag-icons/react/3x2';
import get from 'lodash/get';
import includes from 'lodash/includes';
import isFunction from 'lodash/isFunction';
import join from 'lodash/join';
import map from 'lodash/map';
import toLower from 'lodash/toLower';
import React, { useMemo } from 'react';
import { Badge } from 'reactstrap';
import Link from 'next/link';
import split from 'lodash/split';
import styles from './country-flag.module.css';
import bugsnagClient from '../services/bugsnag';
import useNextHref from '../utils/use-next-href';
import t from '../utils/t';

const countriesToRename = {
  Swaziland: 'Eswatini',
  'West Germany': 'Germany',
  UK: 'United Kingdom',
  USA: 'United States',
  Bahamas: 'The Bahamas',
  'Republic of North Macedonia': 'North Macedonia',
  Congo: 'Republic of the Congo',
  'North Vietnam': 'Vietnam',
  'The Democratic Republic of Congo': 'Democratic Republic of Congo',
};

const nonStandardFlags = {
  'East Germany': '/flags/east-germany.png',
  'Federal Republic of Yugoslavia': '/flags/fry.png',
  Kosovo: '/flags/kosovo.png',
  Palestine: '/flags/palestine.png',
  'Serbia and Montenegro': '/flags/serbia-and-montenegro.png',
  'Soviet Union': '/flags/soviet-union.png',
  Yugoslavia: '/flags/yugoslavia.png',
  Zaire: '/flags/zaire.png',
};

const nonStandardCountryCodes = {
  'Hong Kong': 'HK',
  Czechoslovakia: 'CZ',
};

export default function CountryFlag(props) {
  let country = get(props, 'country');
  country = join(
    map(split(country, ' '), (word) => {
      if (includes(['Of', 'And'], word)) {
        return toLower(word);
      }
      return word;
    }),
    ' ',
  );
  const countryToLookup = get(countriesToRename, country, country);
  const queryObject = useMemo(() => ({ country }), [country]);
  const href = useNextHref(queryObject, '/movies', true);
  if (!country) {
    return null;
  }
  let flag;
  if (nonStandardFlags[countryToLookup]) {
    flag = (
      <a href={href}>
        <img
          alt={countryToLookup}
          className={styles.countryFlag}
          src={nonStandardFlags[countryToLookup]}
          title={t(`country.${country}`)}
        />
      </a>
    );
  } else {
    let countryCode = get(countryCodeLookup.byCountry(countryToLookup), 'iso2');
    if (nonStandardCountryCodes[countryToLookup]) {
      countryCode = nonStandardCountryCodes[countryToLookup];
    }
    if (!countryCode) {
      const warningMessage = `NO COUNTRY CODE: ${country}`;
      bugsnagClient.notify(warningMessage);
      console.warn(warningMessage);
      return (
        <Badge className={styles.countryFlag} href={href} tag="a">
          {country}
        </Badge>
      );
    }
    const Flag = get(Flags, countryCode);
    if (!isFunction(Flag)) {
      const warningMessage = `WRONG COUNTRY CODE for ${country}: ${countryCode}`;
      bugsnagClient.notify(warningMessage);
      console.warn(warningMessage);
      flag = (
        <Badge className={styles.countryFlag} href={href} tag="a">
          {country}
        </Badge>
      );
    } else {
      flag = (
        <a href={href}>
          <Flag
            className={styles.countryFlag}
            title={t(`country.${country}`)}
          />
        </a>
      );
    }
  }
  return <Link href={href}>{flag}</Link>;
}
