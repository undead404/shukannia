import get from 'lodash/get';
import Link from 'next/link';
import React, { useMemo } from 'react';
import { Badge } from 'reactstrap';
import t from '../utils/t';
import styles from './genre-badge.module.css';
import useNextHref from '../utils/use-next-href';

export default function GenreBadge(props) {
  const genre = get(props, 'genre');
  const queryObject = useMemo(() => ({ genre }), [genre]);
  const href = useNextHref(queryObject, '/movies', true);
  return (
    <Link href={href}>
      <a className={styles.genreLink} href={href} title={genre}>
        <Badge
          key={genre}
          className="btn btn-outline-secondary font-weight-normal"
          color="light"
        >
          {t(`genre.${genre}`)}
        </Badge>
      </a>
    </Link>
  );
}
