import React from 'react';
import { render } from 'enzyme';
import GenreBadge from './genre-badge';

jest.mock('next/router', () => ({
  useRouter: () => ({
    asPath: '/',
  }),
}));

describe('genre-badge component', () => {
  it('matches the snapshot', () => {
    expect.assertions(1);
    const app = render(<GenreBadge genre="action" />);
    expect(app).toMatchInlineSnapshot(`
      <a
        class="genreLink"
        href="/movies?genre=action"
        title="action"
      >
        <span
          class="btn btn-outline-secondary font-weight-normal badge badge-light"
        >
          Бойовик
        </span>
      </a>
    `);
  });
});
