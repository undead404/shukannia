import get from 'lodash/get';
import map from 'lodash/map';
import React from 'react';
import GenreBadge from './genre-badge';
import sortByT from '../utils/sort-by-t';

export default function GenreBadges(props) {
  return map(sortByT(get(props, 'genres', []), 'genre'), (genre) => (
    <GenreBadge key={genre} genre={genre} />
  ));
}
