import React from 'react';
import { render } from 'enzyme';
import GenreBadges from './genre-badges';

jest.mock('next/router', () => ({
  useRouter: () => ({
    asPath: '/',
  }),
}));

describe('genre-badges component', () => {
  it('matches the snapshot', () => {
    expect.assertions(1);
    const app = render(<GenreBadges genres={['action']} />);
    expect(app).toMatchInlineSnapshot(`
      <a
        class="genreLink"
        href="/movies?genre=action"
        title="action"
      >
        <span
          class="btn btn-outline-secondary font-weight-normal badge badge-light"
        >
          Бойовик
        </span>
      </a>
    `);
  });
});
