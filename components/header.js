import classNames from 'classnames';
import invoke from 'lodash/invoke';
import Link from 'next/link';
import React, { useState, useCallback } from 'react';
import { Collapse, Nav, Navbar, NavbarToggler, NavItem } from 'reactstrap';

import toTitle from '../utils/to-title';
import LimitForm from './limit-form';
import useQueryObject from '../utils/use-query-object';
import OrderForm from './order-form/order-form';
import styles from './header.module.css';

function stopPropagation(event) {
  invoke(event, 'stopPropagation');
}

export default function Header() {
  const queryObject = useQueryObject();
  const title = toTitle(queryObject);

  const [isOpen, setIsOpen] = useState(false);

  const toggle = useCallback(() => setIsOpen((value) => !value), []);
  const hide = useCallback(() => {
    setIsOpen(false);
  }, []);
  return (
    <Navbar
      className={classNames(styles.header)}
      color="light"
      expand="md"
      light
    >
      <div
        className={classNames(
          'd-flex flex-row flex-nowrap justify-content-stretch',
          styles.brandingWrapper,
        )}
      >
        <div className="d-flex flex-column align-items-center flex-grow-1">
          <Link href="/">
            <a
              className={classNames('navbar-brand', styles.indexLink)}
              href="/"
            >
              <img alt="Шукання" src="/logo200.png" title="Шукання" />
            </a>
          </Link>
          <div className="text-muted text-center">{title}</div>
        </div>
        <NavbarToggler onClick={toggle} />
      </div>
      <Collapse isOpen={isOpen} navbar>
        <Nav className="flex-wrap flex-row" navbar onClick={hide}>
          <NavItem
            className={classNames(styles.navItem, styles.orderFormContainer)}
            onClick={stopPropagation}
          >
            <OrderForm onClick={hide} />
          </NavItem>
          <NavItem
            className={classNames(styles.limitFormContainer, styles.navItem)}
            onClick={stopPropagation}
          >
            <LimitForm />
          </NavItem>
        </Nav>
      </Collapse>
    </Navbar>
  );
}
