import React from 'react';
import { render } from 'enzyme';
import Header from './header';

jest.mock('next/router', () => ({
  useRouter: () => ({
    asPath: '/',
  }),
}));

describe('header component', () => {
  it('matches the snapshot', () => {
    expect.assertions(1);
    const app = render(<Header />);
    expect(app).toMatchInlineSnapshot(`
      <nav
        class="header navbar navbar-expand-md navbar-light bg-light"
      >
        <div
          class="d-flex flex-row flex-nowrap justify-content-stretch brandingWrapper"
        >
          <div
            class="d-flex flex-column align-items-center flex-grow-1"
          >
            <a
              class="navbar-brand indexLink"
              href="/"
            >
              <img
                alt="Шукання"
                src="/logo200.png"
                title="Шукання"
              />
            </a>
            <div
              class="text-muted text-center"
            >
              10 найкращих кінострічок
            </div>
          </div>
          <button
            aria-label="Toggle navigation"
            class="navbar-toggler"
            type="button"
          >
            <span
              class="navbar-toggler-icon"
            />
          </button>
        </div>
        <div
          class="collapse navbar-collapse"
        >
          <ul
            class="flex-wrap flex-row navbar-nav"
          >
            <li
              class="navItem orderFormContainer nav-item"
            >
              <div
                class="orderForm btn-group"
                role="group"
              >
                <a
                  class="btn btn-secondary"
                  href="/movies?order=old-garbage"
                >
                  Поховане часом
                </a>
                <a
                  class="btn btn-warning"
                  href="/movies?order=old-good"
                >
                  Перевірене часом
                </a>
                <a
                  class="btn btn-dark"
                  href="/movies?order=bottom"
                >
                  Найгірше
                </a>
                <a
                  class="btn btn-danger"
                  href="/movies?order=weird"
                >
                  Спірне
                </a>
                <a
                  class="active btn btn-success active"
                  href="/movies"
                >
                  Найкраще
                </a>
                <a
                  class="btn btn-info"
                  href="/movies?order=fresh-garbage"
                >
                  Провальне
                </a>
                <a
                  class="btn btn-primary"
                  href="/movies?order=fresh-good"
                >
                  Актуальне
                </a>
              </div>
            </li>
            <li
              class="limitFormContainer navItem nav-item"
            >
              <form
                class="limitForm form-inline"
              >
                <div
                  class="limitFormGroup form-group"
                >
                  <div
                    class="input-group"
                  >
                    <div
                      class="input-group-prepend"
                    >
                      <span
                        class="input-group-text"
                      >
                        Кількість
                      </span>
                    </div>
                    <input
                      aria-describedby="limit-help"
                      class="limitInput form-control"
                      id="limit"
                      max="100"
                      min="1"
                      type="number"
                      value="10"
                    />
                  </div>
                </div>
                <button
                  class="limitApplyBtn btn btn-secondary"
                >
                  Застосувати
                </button>
              </form>
            </li>
          </ul>
        </div>
      </nav>
    `);
  });
});
