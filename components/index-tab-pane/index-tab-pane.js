import React from 'react';
import { TabPane, Card } from 'reactstrap';
import get from 'lodash/get';
import Link from 'next/link';

export default function IndexTabPane(props) {
  const tabId = get(props, 'tabId');
  const children = get(props, 'children', null);
  return (
    <TabPane tabId={tabId}>
      <Card body>
        {children}

        <Link href="/movies">
          <a className="btn btn-secondary" href="/movies">
            Перейти до списків
          </a>
        </Link>
      </Card>
    </TabPane>
  );
}
