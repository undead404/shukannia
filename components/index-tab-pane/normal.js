import Link from 'next/link';
import React from 'react';
import { CardTitle, CardText } from 'reactstrap';
import IndexTabPane from './index-tab-pane';

export default function NormalIndexTabPane() {
  return (
    <IndexTabPane tabId="normal">
      <CardTitle>Опишу трохи розгорнутіше.</CardTitle>
      <CardText>
        Спершу накладається заданий фільтр (наприклад,{' '}
        <Link href="/movies?country=South%20Korea&genre=animation">
          <a href="/movies?country=South%20Korea&genre=animation">
            анімаційне з Південної Кореї
          </a>
        </Link>
        ). Над такою вибіркою відбуваються подальші дії.
      </CardText>
      <CardText>
        Другий крок - обчислення медіани рейтингу в вибірці. Зазвичай це
        значення близько 66 із 100 (кіномани менш строгі до кіно, ніж було б
        слід).
      </CardText>
      <CardText>
        Третій крок - для кожного елемента вибірки обчислюється вага. При
        отриманні найкращого ця вага - кількість голосів, помножена на різницю
        рейтингу і медіани рейтингів.
      </CardText>
      <CardText>
        Четвертий крок - сортування вибірки за спаданням ваги і обрізання
        вибірки до розміру N (за замовчуванням 10, зазвичай не більше 100).
      </CardText>
      <CardText>
        П&apos;ятий крок - сортування результату за зростанням дати виходу.
      </CardText>
      <CardText>
        При інших критеріях сортування міняється лише спосіб обчислення ваги в
        третьому кроці. Наразі це реалізовано за допомогою MongoDB aggregate.
      </CardText>
    </IndexTabPane>
  );
}
