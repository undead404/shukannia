import Link from 'next/link';
import React from 'react';
import { CardTitle, CardText } from 'reactstrap';
import IndexTabPane from './index-tab-pane';

export default function ShortIndexTabPane() {
  return (
    <IndexTabPane tabId="short">
      <CardTitle>
        Опишу все ж таки, що таке{' '}
        <Link href="/movies">
          <a href="/movies">Шукання</a>
        </Link>
        .
      </CardTitle>
      <CardText>
        Це агрегатор IMDb, котрий динамічно виводить стислі списки найкращих{' '}
        <Link href="/movies?kind=feature">
          <a href="/movies?kind=feature">кінострічок</a>
        </Link>
        ,{' '}
        <Link href="/movies?kind=tv_series">
          <a href="/movies?kind=tv_series">телесеріалів</a>
        </Link>
        ,{' '}
        <Link href="/movies?kind=tv_episode">
          <a href="/movies?kind=tv_episode">телеепізодів</a>
        </Link>
        ,{' '}
        <Link href="/movies?kind=creative_work">
          <a href="/movies?kind=creative_work">творчих робіт</a>
        </Link>{' '}
        і навіть{' '}
        <Link href="/movies?kind=video_game">
          <a href="/movies?kind=video_game">трошки відеоігор</a>
        </Link>{' '}
        згідно з заданими фільтрами.
      </CardText>
      <CardText>
        Бонус: Шукання може виводити також{' '}
        <Link href="/movies?order=bottom">
          <a href="/movies?order=bottom">найгірше</a>
        </Link>{' '}
        і{' '}
        <Link href="/movies?order=weird">
          <a href="/movies?order=weird">спірне</a>
        </Link>{' '}
        кіно.
      </CardText>
      <CardText>
        Ні в IMDb, ні в Кіноріуму такого функціоналу наразі немає.
      </CardText>
    </IndexTabPane>
  );
}
