import classNames from 'classnames';
import ISO_639_1 from 'iso-639-1';
import get from 'lodash/get';
import React, { useMemo } from 'react';
import { Badge } from 'reactstrap';
import Link from 'next/link';
import styles from './language-badge.module.css';
import bugsnagClient from '../../services/bugsnag';
import useNextHref from '../../utils/use-next-href';
import nonStandardLanguages from './non-standard-languages.json';
import t from '../../utils/t';

export default function LanguageBadge(props) {
  const languageName = get(props, 'languageName');
  const languageCode = ISO_639_1.getCode(languageName);
  const localName = t(`language.${languageName}`);
  let nativeLanguageName =
    ISO_639_1.getNativeName(languageCode) ||
    nonStandardLanguages[languageName] ||
    localName;
  if (!nativeLanguageName) {
    const warningMessage = `LANGUAGE CODE NOT FOUND: ${languageName}`;
    console.warn(warningMessage);
    bugsnagClient.notify(warningMessage);
    nativeLanguageName = languageName;
  }
  const queryObject = useMemo(() => ({ language: languageName }), [
    languageName,
  ]);
  const href = useNextHref(queryObject, '/movies', true);
  return (
    <Link href={href}>
      <a
        className={styles.genreLink}
        href={href}
        title={localName || languageName}
      >
        <Badge
          className={classNames(
            'font-weight-normal font-italic',
            styles.languageBadge,
          )}
          color="dark"
        >
          {nativeLanguageName}
        </Badge>
      </a>
    </Link>
  );
}
