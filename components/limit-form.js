import get from 'lodash/get';
import head from 'lodash/head';
import invoke from 'lodash/invoke';
import isNumber from 'lodash/isNumber';
import pickBy from 'lodash/pickBy';
import split from 'lodash/split';
import { useRouter } from 'next/router';
import React, { useCallback, useState } from 'react';
import {
  Button,
  Form,
  FormGroup,
  InputGroupAddon,
  InputGroupText,
  Input,
  InputGroup,
} from 'reactstrap';
import { DEFAULT_LIMIT, DEFAULT_PARAMETERS } from '../config';
import useQueryObject from '../utils/use-query-object';
import styles from './limit-form.module.css';
import t from '../utils/t';

function useUserInput(initialValue) {
  const [value, setValue] = useState(initialValue);
  const handle = useCallback(
    (event) => setValue(get(event, 'target.value', initialValue)),
    [initialValue],
  );
  return [value, handle];
}
export default function LimitForm() {
  const router = useRouter();
  const activeLimit = get(router, 'query.limit', DEFAULT_LIMIT);
  const [limit, handleLimitChange] = useUserInput(activeLimit);
  const queryObject = useQueryObject();
  const handleSumbit = useCallback(
    (event) => {
      invoke(event, 'preventDefault');
      const newQueryObject = pickBy({ ...queryObject, limit }, (value, key) => {
        const defaultValue = DEFAULT_PARAMETERS.get(key);
        if (isNumber(defaultValue)) {
          return Number.parseInt(value) !== defaultValue;
        }
        return defaultValue !== value;
      });
      router.push(
        {
          pathname: router.pathname,
          query: newQueryObject,
        },
        {
          pathname: head(split(router.asPath, '?')),
          query: newQueryObject,
        },
      );
    },
    [limit, router, queryObject],
  );
  return (
    <Form className={styles.limitForm} inline onSubmit={handleSumbit}>
      <FormGroup className={styles.limitFormGroup}>
        <InputGroup>
          <InputGroupAddon addonType="prepend">
            <InputGroupText>{t('Limit')}</InputGroupText>
          </InputGroupAddon>
          <Input
            aria-describedby="limit-help"
            className={styles.limitInput}
            id="limit"
            max="100"
            min="1"
            onChange={handleLimitChange}
            type="number"
            value={limit}
          />
        </InputGroup>
      </FormGroup>
      <Button className={styles.limitApplyBtn} color="secondary">
        {t('Apply')}
      </Button>
    </Form>
  );
}
