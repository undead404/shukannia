import React from 'react';
import { render } from 'enzyme';
import LimitForm from './limit-form';

jest.mock('next/router', () => ({
  useRouter: () => ({
    asPath: '/',
  }),
}));

describe('limit-form component', () => {
  it('matches the snapshot', () => {
    expect.assertions(1);
    const app = render(<LimitForm />);
    expect(app).toMatchInlineSnapshot(`
      <form
        class="limitForm form-inline"
      >
        <div
          class="limitFormGroup form-group"
        >
          <div
            class="input-group"
          >
            <div
              class="input-group-prepend"
            >
              <span
                class="input-group-text"
              >
                Кількість
              </span>
            </div>
            <input
              aria-describedby="limit-help"
              class="limitInput form-control"
              id="limit"
              max="100"
              min="1"
              type="number"
              value="10"
            />
          </div>
        </div>
        <button
          class="limitApplyBtn btn btn-secondary"
        >
          Застосувати
        </button>
      </form>
    `);
  });
});
