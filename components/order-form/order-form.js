import get from 'lodash/get';
import React from 'react';
import { ButtonGroup } from 'reactstrap';
import map from 'lodash/map';
import useQueryObject from '../../utils/use-query-object';
import styles from './order-form.module.css';
import OrderLink from './order-link';

const ORDERS = [
  'old-garbage',
  'old-good',
  'bottom',
  'weird',
  'top',
  'fresh-garbage',
  'fresh-good',
];
export default function OrderForm(props) {
  const onClick = get(props, 'onClick');
  const queryObject = useQueryObject();
  const activeOrder = get(queryObject, 'order', 'top');
  return (
    <ButtonGroup className={styles.orderForm} onClick={onClick}>
      {map(ORDERS, (order) => (
        <OrderLink key={order} isActive={activeOrder === order} order={order} />
      ))}
    </ButtonGroup>
  );
}
