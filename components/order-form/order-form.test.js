import React from 'react';
import { render } from 'enzyme';
import OrderForm from './order-form';

jest.mock('next/router', () => ({
  useRouter: () => ({
    asPath: '/',
  }),
}));

describe('order-form component', () => {
  it('matches the snapshot', () => {
    expect.assertions(1);
    const app = render(<OrderForm />);
    expect(app).toMatchInlineSnapshot(`
      <div
        class="orderForm btn-group"
        role="group"
      >
        <a
          class="btn btn-secondary"
          href="/movies?order=old-garbage"
        >
          Поховане часом
        </a>
        <a
          class="btn btn-warning"
          href="/movies?order=old-good"
        >
          Перевірене часом
        </a>
        <a
          class="btn btn-dark"
          href="/movies?order=bottom"
        >
          Найгірше
        </a>
        <a
          class="btn btn-danger"
          href="/movies?order=weird"
        >
          Спірне
        </a>
        <a
          class="active btn btn-success active"
          href="/movies"
        >
          Найкраще
        </a>
        <a
          class="btn btn-info"
          href="/movies?order=fresh-garbage"
        >
          Провальне
        </a>
        <a
          class="btn btn-primary"
          href="/movies?order=fresh-good"
        >
          Актуальне
        </a>
      </div>
    `);
  });
});
