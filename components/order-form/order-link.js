import classNames from 'classnames';
import get from 'lodash/get';
import Link from 'next/link';
import React, { useMemo } from 'react';
import { Button } from 'reactstrap';
import useNextHref from '../../utils/use-next-href';
import t from '../../utils/t';
import styles from './order-link.module.css';

const ordersColors = new Map([
  ['old-garbage', 'secondary'],
  ['old-good', 'warning'],
  ['bottom', 'dark'],
  ['weird', 'danger'],
  ['top', 'success'],
  ['fresh-garbage', 'info'],
  ['fresh-good', 'primary'],
]);

export default function OrderLink(props) {
  const order = get(props, 'order');
  const isActive = get(props, 'isActive');
  const newParameters = useMemo(() => ({ order }), [order]);
  const href = useNextHref(newParameters, '/movies', true);
  return (
    <Link href={href}>
      <Button
        active={isActive}
        className={classNames({
          [styles.active]: isActive,
        })}
        color={ordersColors.get(order)}
        href={href}
      >
        {t(`order.${order}`)}
      </Button>
    </Link>
  );
}
