import get from 'lodash/get';
import React from 'react';
import styles from './rank.module.css';

const GLOBAL_VALUE_LIMIT = 1000000;
const GLOBAL_LIMIT = 100000;
const REGIONAL_LIMIT = 10000;
const AUTHOR_LIMIT = 1000;
const AMATEUR_LIMIT = 100;

export default function Rank(props) {
  const votesNumber = get(props, 'votesNum', 0);
  let iconSource;
  let title;
  if (votesNumber > GLOBAL_VALUE_LIMIT) {
    iconSource = '/ranks/1m.png';
    title = 'Всесвітнє надбання';
  } else if (votesNumber > GLOBAL_LIMIT) {
    iconSource = '/ranks/100k.png';
    title = 'Всесвітнє визнання';
  } else if (votesNumber > REGIONAL_LIMIT) {
    iconSource = '/ranks/10k.png';
    title = 'Нішеве визнання';
  } else if (votesNumber > AUTHOR_LIMIT) {
    iconSource = '/ranks/1k.png';
    title = 'Авторське кіно';
  } else if (votesNumber > AMATEUR_LIMIT) {
    iconSource = '/ranks/100.png';
    title = 'Аматорське кіно';
  } else {
    iconSource = '/ranks/0.png';
    title = 'Ніхто про це не чув...';
  }
  return (
    <img alt={title} className={styles.rank} src={iconSource} title={title} />
  );
}
