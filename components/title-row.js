import classNames from 'classnames';
import get from 'lodash/get';
import isNil from 'lodash/isNil';
import map from 'lodash/map';
import padStart from 'lodash/padStart';
import React, { useState, useCallback } from 'react';
import { Media } from 'reactstrap';
import toImdbLink from '../utils/to-imdb-link';
import GenreBadges from './genre-badges';
import styles from './title-row.module.css';
import CountryFlag from './country-flag';
import proxyImage from '../utils/proxy-image';
import Rank from './rank';
import LanguageBadge from './language-badge/language-badge';

function toEpisodeName(episode, series) {
  return `${series.name} S${padStart(`${episode.season}`, 2, '0')}E${padStart(
    `${episode.episode}`,
    2,
    '0',
  )} ("${episode.name}")`;
}

const FIFTH_LIMIT = 80;
const FOURTH_LIMIT = 75;
const THIRD_LIMIT = 66;
const SECOND_LIMIT = 50;

export default function TitleRow(props) {
  const explicit = get(props, 'explicit');
  const imdbId = get(props, 'imdbId');
  const series = get(props, 'series');
  const name = series ? toEpisodeName(props, series) : get(props, 'name', '');
  const severe = get(props, 'severe', false);
  const genres = get(props, 'genres', []);
  const releasedAt = new Date(get(props, 'releasedAt')).toLocaleDateString(
    'uk-UA',
  );
  const rating = get(props, 'rating', 0);
  const imageSource = get(props, 'image');
  const smallImageSource = imageSource
    ? proxyImage(imageSource)
    : 'https://via.placeholder.com/200';
  const countries = get(props, 'countries', []);
  const [isActive, setIsActive] = useState(false);
  const setActive = useCallback(() => setIsActive(true), []);
  const setInactive = useCallback(() => setIsActive(false), []);
  const isUnrated = isNil(severe) && !explicit;
  const votesNumber = get(props, 'votesNum');
  const languages = get(props, 'languages', []);
  return (
    <div
      className={classNames(
        styles.title,
        'title alert border border-secondary',
        {
          [styles.active]: isActive,
          'alert-danger border-danger': severe,
          'alert-secondary': isUnrated,
          'alert-warning border-warning': explicit && !severe,
          [styles.titleOk]:
            !isNil(severe) && !severe && !isNil(explicit) && !explicit,
        },
      )}
      onMouseEnter={setActive}
      onMouseLeave={setInactive}
      onTouchCancel={setInactive}
      onTouchEnd={setInactive}
      onTouchStart={setActive}
    >
      <Media
        title={
          (severe && 'Обережно, ця стрічка відома жорсткістю') ||
          (explicit &&
            'Ця стрічка не відома жорсткістю, але має "дорослий" рейтинг') ||
          (isUnrated && 'Віковий рейтинг цієї стрічки невідомий') ||
          null
        }
      >
        <Media
          className={styles.imageForDesktop}
          href={toImdbLink(imdbId)}
          left
          rel="noopener noreferrer"
          target="_blank"
          title="IMDb"
        >
          <Media className={styles.mainImage} object src={smallImageSource} />
          <div className={styles.ratingContainer}>
            <div
              className={classNames(styles.rating, {
                'text-success': rating > FIFTH_LIMIT,
                'text-primary': rating > FOURTH_LIMIT && rating <= FIFTH_LIMIT,
                'text-info': rating > THIRD_LIMIT && rating <= FOURTH_LIMIT,
                'text-warning': rating > SECOND_LIMIT && rating <= THIRD_LIMIT,
                'text-danger': rating <= SECOND_LIMIT,
              })}
            >
              {rating}%
            </div>
          </div>
        </Media>
        <Media body>
          <Media heading>{name}</Media>
          <a
            className={styles.imageForMobile}
            href={toImdbLink(imdbId)}
            rel="noopener noreferrer"
            target="_blank"
            title="IMDb"
          >
            <Media className={styles.mainImage} object src={smallImageSource} />
            <div className={styles.ratingContainer}>
              <div
                className={classNames(styles.rating, {
                  'text-success': rating > FIFTH_LIMIT,
                  'text-primary':
                    rating > FOURTH_LIMIT && rating <= FIFTH_LIMIT,
                  'text-info': rating > THIRD_LIMIT && rating <= FOURTH_LIMIT,
                  'text-warning':
                    rating > SECOND_LIMIT && rating <= THIRD_LIMIT,
                  'text-danger': rating <= SECOND_LIMIT,
                })}
              >
                {rating}%
              </div>
            </div>
          </a>
          <p className={styles.miscData}>
            <span className={styles.releasedAt}>{releasedAt}</span>
            <GenreBadges genres={genres} />
            {map(languages, (language) => (
              <LanguageBadge key={language} languageName={language} />
            ))}
            {map(countries, (country) => (
              <CountryFlag key={country} country={country} />
            ))}
          </p>
          <p>{get(props, 'description')}</p>
        </Media>
      </Media>
      {series && (
        <TitleRow
          countries={series.countries}
          description={series.description}
          explicit={series.explicit}
          genres={series.genres}
          image={series.image}
          imdbId={series.imdbId}
          languages={series.languages}
          name={series.name}
          rating={series.rating}
          releasedAt={series.releasedAt}
          series={series.series}
          severe={series.severe}
          votesNum={series.votesNum}
        />
      )}
      <Rank votesNum={votesNumber} />
    </div>
  );
}
