import React from 'react';
import { render } from 'enzyme';
import TitleRow from './title-row';

jest.mock('next/router', () => ({
  useRouter: () => ({
    asPath: '/',
  }),
}));
jest.mock('../services/bugsnag', () => ({
  notify: jest.fn(),
}));
describe('title-row component', () => {
  it('matches the snapshot', () => {
    expect.assertions(1);
    const app = render(
      <TitleRow
        explicit
        genres={['action', 'sci_fi']}
        imdbId={1}
        languages={['Ukrainian', 'Spanish']}
        name="Ololo"
        rating={99}
        releasedAt="2000-01-01"
        severe={0}
        votesNum={161}
      />,
    );
    expect(app).toMatchInlineSnapshot(`
      <div
        class="title title alert border border-secondary alert-warning border-warning"
      >
        <div
          class="media"
          title="Ця стрічка не відома жорсткістю, але має \\"дорослий\\" рейтинг"
        >
          <a
            class="imageForDesktop media-left"
            href="https://imdb.com/title/tt0000001"
            rel="noopener noreferrer"
            target="_blank"
            title="IMDb"
          >
            <img
              class="mainImage media-object"
              src="https://via.placeholder.com/200"
            />
            <div
              class="ratingContainer"
            >
              <div
                class="rating text-success"
              >
                99%
              </div>
            </div>
          </a>
          <div
            class="media-body"
          >
            <h4
              class="media-heading"
            >
              Ololo
            </h4>
            <a
              class="imageForMobile"
              href="https://imdb.com/title/tt0000001"
              rel="noopener noreferrer"
              target="_blank"
              title="IMDb"
            >
              <img
                class="mainImage media-object"
                src="https://via.placeholder.com/200"
              />
              <div
                class="ratingContainer"
              >
                <div
                  class="rating text-success"
                >
                  99%
                </div>
              </div>
            </a>
            <p
              class="miscData"
            >
              <span
                class="releasedAt"
              >
                01.01.2000
              </span>
              <a
                class="genreLink"
                href="/movies?genre=action"
                title="action"
              >
                <span
                  class="btn btn-outline-secondary font-weight-normal badge badge-light"
                >
                  Бойовик
                </span>
              </a>
              <a
                class="genreLink"
                href="/movies?genre=sci_fi"
                title="sci_fi"
              >
                <span
                  class="btn btn-outline-secondary font-weight-normal badge badge-light"
                >
                  Науково-фантастичне
                </span>
              </a>
              <a
                class="genreLink"
                href="/movies?language=Ukrainian"
                title="Українська"
              >
                <span
                  class="font-weight-normal font-italic languageBadge badge badge-dark"
                >
                  Українська
                </span>
              </a>
              <a
                class="genreLink"
                href="/movies?language=Spanish"
                title="Іспанська"
              >
                <span
                  class="font-weight-normal font-italic languageBadge badge badge-dark"
                >
                  Español
                </span>
              </a>
            </p>
            <p />
          </div>
        </div>
        <img
          alt="Аматорське кіно"
          class="rank"
          src="/ranks/100.png"
          title="Аматорське кіно"
        />
      </div>
    `);
  });
});
