import sortBy from 'lodash/sortBy';

const GENRES = sortBy([
  'documentary',
  'short',
  'animation',
  'comedy',
  'romance',
  'sport',
  'news',
  'family',
  'fantasy',
  'drama',
  'horror',
  'war',
  'crime',
  'western',
  'sci_fi',
  'biography',
  'adventure',
  'history',
  'action',
  'music',
  'mystery',
  'thriller',
  'musical',
  'film_noir',
  'game_show',
  'talk_show',
  'reality_tv',
  'adult',
]);
const KINDS = sortBy([
  'documentary',
  'feature',
  'short',
  'tv_episode',
  'tv_movie',
  'tv_series',
  'tv_short',
  'tv_special',
  'video',
]);
const API_URL = 'https://shukannia-api.herokuapp.com';
const DEFAULT_LIMIT = 10;

const DEFAULT_PARAMETERS = new Map([
  ['kind', '!tv_series,!video_game'],
  ['limit', DEFAULT_LIMIT],
  ['order', 'top'],
]);
export { API_URL, DEFAULT_LIMIT, DEFAULT_PARAMETERS, GENRES, KINDS };
