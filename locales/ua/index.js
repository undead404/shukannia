import countries from './countries.json';
import genres from './genres.json';
import kinds from './kinds.json';
import languages from './languages.json';
import orders from './orders.json';

export default {
  country: countries,
  countries: 'Країни',
  filters: {
    country: 'Країни',
    genre: 'Жанри',
    kind: 'Типи',
    language: 'Мови',
  },
  genre: genres,
  genres: 'Жанри',
  kind: kinds,
  kinds: 'Типи',
  language: languages,
  languages: 'Мови',
  releasedAt: 'Дата випуску',
  title: 'Назва',
  Shukannia: 'Шукання',
  Limit: 'Кількість',
  Apply: 'Застосувати',
  order: orders,
};
