const {
  BugsnagBuildReporterPlugin,
  BugsnagSourceMapUploaderPlugin,
} = require('webpack-bugsnag-plugins');
const { ProvidePlugin } = require('webpack');
const packageInfo = require('./package.json');

let appVersion = packageInfo.version;
if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
  appVersion += '-dev';
}
const buildDetails = {
  apiKey: '1043d368f6ea55c390bf2bad246f2cae',
  appVersion,
  releaseStage: process.env.NODE_ENV || 'development',
};
const plugins = [
  new ProvidePlugin({
    Holder: 'holderjs',
    holder: 'holderjs',
    'window.Holder': 'holderjs',
  }),
  new BugsnagBuildReporterPlugin(buildDetails),
];
if (process.env.NODE_ENV === 'production') {
  plugins.push(
    new BugsnagSourceMapUploaderPlugin({
      ...buildDetails,
      overwrite: true,
      publicPath: packageInfo.homepage,
    }),
  );
}
module.exports = {
  devtool: 'hidden-source-map',
  env: {
    BUGSNAG_API_KEY: '1043d368f6ea55c390bf2bad246f2cae',
  },
  webpack(config) {
    config.plugins.push(...plugins);
    return config;
  },
};
