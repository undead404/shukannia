import bugsnagReact from '@bugsnag/plugin-react';
import { Router } from 'next/router';
import PropTypes from 'prop-types';
import React, { useState, useCallback, useEffect } from 'react';
import Loader from 'react-loader-spinner';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';
import { pageview } from '../utils/ga';
import './app.css';
import bugsnagClient from '../services/bugsnag';

bugsnagClient.use(bugsnagReact, React);
const ErrorBoundary = bugsnagClient.getPlugin('react');
if (process.env.NODE_ENV === 'production') {
  Router.events.on('routeChangeComplete', pageview);
}
function Shukannia({ Component, pageProps }) {
  const [isLoading, setIsLoading] = useState(false);
  const onRouteChangeStart = useCallback(() => {
    setIsLoading(true);
  }, []);
  const onRouteChangeComplete = useCallback(() => {
    setIsLoading(false);
  }, []);
  useEffect(() => {
    Router.events.on('routeChangeStart', onRouteChangeStart);
    Router.events.on('routeChangeComplete', onRouteChangeComplete);
    return () => {
      Router.events.off('routeChangeStart', onRouteChangeStart);
      Router.events.off('routeChangeComplete', onRouteChangeComplete);
    };
  }, [onRouteChangeComplete, onRouteChangeStart]);
  return (
    <ErrorBoundary>
      <Loader
        className="loader"
        color="#00BFFF"
        height={300}
        timeout={3000}
        type="TailSpin"
        visible={isLoading}
        width={300} // 3 secs
      />
      {/* eslint-disable-next-line react/jsx-props-no-spreading */}
      <Component isLoading={isLoading} {...pageProps} />
    </ErrorBoundary>
  );
}
Shukannia.propTypes = {
  Component: PropTypes.func.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  pageProps: PropTypes.object.isRequired,
};
export default Shukannia;
