import classNames from 'classnames';
import isEmpty from 'lodash/isEmpty';
import Head from 'next/head';
import { parseUrl } from 'query-string';
import React, { useEffect, useState, useCallback } from 'react';
import { Container, Nav, NavItem, NavLink, TabContent } from 'reactstrap';
import get from 'lodash/get';
import Router from 'next/router';
import Link from 'next/link';
import styles from './index.module.css';

import t from '../utils/t';
import ShortIndexTabPane from '../components/index-tab-pane/short';
import NormalIndexTabPane from '../components/index-tab-pane/normal';
import LongIndexTabPane from '../components/index-tab-pane/long';

const DESCRIPTION =
  'Шукання дає змогу переглядати списки найкращого, найгіршого та спірного кіно з фільтруванням за напрямами, типами та країнами на основі оцінок IMDb';

function Index(props) {
  const query = get(props, 'query');
  const title = t('Shukannia');
  useEffect(() => {
    if (!isEmpty(query)) {
      // eslint-disable-next-line lodash/prefer-lodash-method
      Router.replace('/movies', { pathname: '/movies', query });
    }
  }, [query]);
  const [activeTab, setActiveTab] = useState('short');

  const showShort = useCallback(() => setActiveTab('short'), []);
  const showNormal = useCallback(() => setActiveTab('normal'), []);
  const showLong = useCallback(() => setActiveTab('long'), []);

  return (
    <div className={classNames('container-fluid', styles.mainPage)}>
      <Head>
        <title>{title}</title>
        <meta content={DESCRIPTION} name="description" />
        <meta content="Vitalii Perehonchuk" name="author" />
        <meta content="Vitalii Perehonchuk" name="owner" />
        <meta content="public" httpEquiv="Cache-Control" />
        <meta content={title} name="og:title" />
        <meta content="video.rates" name="og:type" />
        <meta content="/preview.png" name="og:image" />
        <meta content="Шукання" name="og:site_name" />
        <meta content="Ukraine" name="og:country-name" />
        {/* <!-- Open Graph / Facebook --> */}
        <meta content="https://shukannia.now.sh" property="og:url" />
        <meta content={DESCRIPTION} property="og:description" />
        {/* <!-- Twitter --> */}
        <meta content="summary_large_image" property="twitter:card" />
        <meta content="https://shukannia.now.sh" property="twitter:url" />
        <meta content={title} property="twitter:title" />
        <meta content={DESCRIPTION} property="twitter:description" />
        <meta content="/preview.png" property="twitter:image" />

        <meta content="Static" name="document-state" />
      </Head>

      <main>
        <Container className="align-items-stretch">
          <Link href="/movies">
            <a className={styles.linkToMovies} href="/movies">
              <img alt="Шукання" src="/logo.png" title="До пошуку" />
            </a>
          </Link>
          <Nav tabs>
            <NavItem>
              <NavLink
                className={classNames({ active: activeTab === 'short' })}
                onClick={showShort}
              >
                Коротко
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classNames({ active: activeTab === 'normal' })}
                onClick={showNormal}
              >
                Розгорнуто
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classNames({ active: activeTab === 'long' })}
                onClick={showLong}
              >
                Іще розгорнутіше
              </NavLink>
            </NavItem>
          </Nav>
          <TabContent activeTab={activeTab}>
            <ShortIndexTabPane />
            <NormalIndexTabPane />
            <LongIndexTabPane />
          </TabContent>
        </Container>
      </main>
    </div>
  );
}
Index.getInitialProps = (context) => {
  const { query } = context.req ? parseUrl(context.req.url) : context;
  return { query };
};

export default Index;
