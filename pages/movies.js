import classNames from 'classnames';
import get from 'lodash/get';
import head from 'lodash/head';
import map from 'lodash/map';
import nth from 'lodash/nth';
import pickBy from 'lodash/pickBy';
import split from 'lodash/split';
import Head from 'next/head';
import queryString from 'query-string';
import React from 'react';
import { Container } from 'reactstrap';
import sortBy from 'lodash/sortBy';
import getFromApi from '../services/api';
import TitleRow from '../components/title-row';
import styles from './movies.module.css';
import useQueryObject from '../utils/use-query-object';
import t from '../utils/t';
import proxyImage from '../utils/proxy-image';
import toTitle from '../utils/to-title';
import Header from '../components/header';
import AdvancedFilter from '../components/advanced-filter/advanced-filter';
import useNextHref from '../utils/use-next-href';

const PREVIEW_OPTIONS = {
  w: 600,
  h: 314,
  cbg: 'black',
  fit: 'contain',
};

const DESCRIPTION =
  'Шукання дає змогу переглядати списки найкращого, найгіршого та спірного кіно з фільтруванням за напрямами, типами та країнами на основі оцінок IMDb';

function Movies(props) {
  const movies = get(props, 'movies', []);
  const queryObject = useQueryObject();
  let previewImage = get(
    head(sortBy(movies, (movie) => -movie.weight)),
    'image',
  );
  if (previewImage) {
    previewImage = proxyImage(previewImage, PREVIEW_OPTIONS);
  }
  const title = `${t('Shukannia')} | ${toTitle(queryObject)}`;
  const href = useNextHref(null, '/movies');
  return (
    <div className={classNames('container-fluid', styles.mainPage)}>
      <Head>
        <title>{title}</title>
        <meta content={DESCRIPTION} name="description" />
        <meta content="Vitalii Perehonchuk" name="author" />
        <meta content="Vitalii Perehonchuk" name="owner" />
        <meta content="public" httpEquiv="Cache-Control" />
        <meta content={title} name="og:title" />
        <meta content="video.rates" name="og:type" />
        {previewImage && <meta content={previewImage} name="og:image" />}
        <meta content="Шукання" name="og:site_name" />

        <meta content="Ukraine" name="og:country-name" />

        {/* <!-- Open Graph / Facebook --> */}
        <meta content={href} property="og:url" />
        <meta content={DESCRIPTION} property="og:description" />

        {/* <!-- Twitter --> */}
        <meta content="summary_large_image" property="twitter:card" />
        <meta content={href} property="twitter:url" />
        <meta content={title} property="twitter:title" />
        <meta content={DESCRIPTION} property="twitter:description" />
        {previewImage && (
          <meta content={previewImage} property="twitter:image" />
        )}
        <meta content="Dynamic" name="document-state" />
      </Head>

      <main>
        <Header />
        <AdvancedFilter />
        <Container>
          {map(movies, (movie) => (
            <TitleRow
              key={movie._id}
              countries={movie.countries}
              description={movie.description}
              episode={movie.episode}
              explicit={movie.explicit}
              genres={movie.genres}
              image={movie.image}
              imdbId={movie.imdbId}
              languages={movie.languages}
              name={movie.name}
              rating={movie.rating}
              releasedAt={movie.releasedAt}
              season={movie.season}
              series={movie.series}
              severe={movie.severe}
              votesNum={movie.votesNum}
            />
          ))}
        </Container>
      </main>
    </div>
  );
}
Movies.getInitialProps = async (context) => {
  const queryObject = pickBy(
    queryString.parse(nth(split(context.asPath, '?'), 1)),
  );
  const movies = await getFromApi(queryObject);
  return { movies };
};

export default Movies;
