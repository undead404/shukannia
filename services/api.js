import isNumber from 'lodash/isNumber';
import mapValues from 'lodash/mapValues';
import fetch from 'node-fetch';
import queryString from 'query-string';
import { API_URL, DEFAULT_PARAMETERS } from '../config';

const MINUTES_IN_HOUR = 60;
const SECONDS_IN_MINUTE = 60;
const MILLISECONDS_IN_SECOND = 1000;
const EXPIRES_AFTER =
  MINUTES_IN_HOUR * SECONDS_IN_MINUTE * MILLISECONDS_IN_SECOND;

async function maybeFromSessionStorage(key, acquire) {
  if (process.browser) {
    const cachedAt = sessionStorage.getItem(`${key}:cached_at`);
    if (!cachedAt || new Date() - cachedAt > EXPIRES_AFTER) {
      const result = await acquire();
      sessionStorage.setItem(key, result);
      sessionStorage.setItem(`${key}:cached_at`, new Date().valueOf());
    }
    return sessionStorage.getItem(key);
  }
  return acquire();
}

export default async function getFromApi(queryObject) {
  const queryData = mapValues(queryObject, (value, key) => {
    const defaultValue = DEFAULT_PARAMETERS.get(key);
    if (isNumber(defaultValue)) {
      if (defaultValue === Number.parseInt(value)) {
        return undefined;
      }
      return value;
    }
    if (defaultValue === value) {
      return undefined;
    }
    return value;
  });
  const url = queryString.stringifyUrl({ url: API_URL, query: queryData });
  try {
    const data = await maybeFromSessionStorage(url, async () => {
      const response = await fetch(url);
      return response.text();
    });
    return JSON.parse(data);
  } catch (error) {
    console.error(error);
    const response = await fetch(url);
    return response.json();
  }
}
