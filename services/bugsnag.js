import bugsnag from '@bugsnag/js';
import package_ from '../package.json';

let appVersion = package_.version;
if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
  appVersion += '-dev';
}
const bugsnagClient = bugsnag({
  apiKey: process.env.BUGSNAG_API_KEY,
  appVersion,
});
export default bugsnagClient;
