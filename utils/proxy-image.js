import { stringifyUrl } from 'query-string';

const DEFAULT_PREVIEW_WIDTH = 300;
function sortKeys(key1, key2) {
  if (key1 === 'url') return -1;
  if (key2 === 'url') return 1;
  return 0;
}

export default function proxyImage(
  imageSource,
  options = { w: DEFAULT_PREVIEW_WIDTH },
) {
  return stringifyUrl(
    {
      url: '//images.weserv.nl',
      query: { url: imageSource, ...options },
    },
    {
      sort: sortKeys,
    },
  );
}
