import compact from 'lodash/compact';
import invoke from 'lodash/invoke';
import join from 'lodash/join';
import t from './t';

function getKey(baseKey, key) {
  return join(compact([baseKey, key]), '.');
}

export default function sortByT(collection, baseKey) {
  return invoke(collection, 'sort', (item1, item2) =>
    invoke(
      t(getKey(baseKey, item1)),
      'localeCompare',
      t(getKey(baseKey, item2)),
    ),
  );
}
