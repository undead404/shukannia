import get from 'lodash/get';
import ua from '../locales/ua';

export default function t(key) {
  return get(ua, key);
}
