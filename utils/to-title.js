import compact from 'lodash/compact';
import get from 'lodash/get';
import join from 'lodash/join';
import { DEFAULT_PARAMETERS } from '../config';

const QUALITIES = new Map([
  ['top', 'найкращих'],
  ['bottom', 'найгірших'],
  ['old-garbage', 'похованих часом'],
  ['old-good', 'перевірених часом'],
  ['weird', 'спірних'],
  ['fresh-garbage', 'провальних'],
  ['fresh-good', 'актуальних'],
]);
const KINDS = new Map([
  ['feature', 'кінострічок'],
  ['tv_series', 'телесеріалів'],
  ['tv_episode', 'телеепізодів'],
  ['creative_work', 'творчих робіт'],
  ['video_game', 'відеоігор'],
]);
export default function toTitle(options) {
  const limit = get(options, 'limit', DEFAULT_PARAMETERS.get('limit'));
  const quality = QUALITIES.get(get(options, 'order', 'top'));
  const kind = KINDS.get(get(options, 'kind', 'feature'));
  const genre = get(options, 'genre');
  return join(compact([limit, quality, kind, genre && `#${genre}`]), ' ');
}
