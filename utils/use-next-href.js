import forEach from 'lodash/forEach';
import { useMemo } from 'react';
import { stringifyUrl } from 'query-string';
import { DEFAULT_PARAMETERS } from '../config';
import useParsedUrl from './use-parsed-url';

export default function useNextHref(parameters, pathname, invertable = false) {
  const { query: currentQueryObject } = useParsedUrl();
  const newQueryObject = useMemo(() => {
    const nqo = {
      ...currentQueryObject,
    };
    forEach(parameters, (value, key) => {
      if (
        !value ||
        value === DEFAULT_PARAMETERS.get(key) ||
        (invertable && nqo[key] === value)
      ) {
        delete nqo[key];
      } else {
        nqo[key] = value;
      }
    });
    return nqo;
  }, [currentQueryObject, parameters, invertable]);
  return stringifyUrl({ url: pathname, query: newQueryObject });
}
