import { useRouter } from 'next/router';
import { parseUrl } from 'query-string';
import { useMemo } from 'react';

export default function useParsedUrl() {
  const { asPath } = useRouter();
  return useMemo(() => parseUrl(asPath), [asPath]);
}
