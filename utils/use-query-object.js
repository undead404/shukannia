import nth from 'lodash/nth';
import split from 'lodash/split';
import { useRouter } from 'next/router';
import { parse } from 'query-string';
import { useMemo } from 'react';

export default function useQueryObject() {
  const router = useRouter();
  const { asPath } = router;
  const queryObject = useMemo(() => parse(nth(split(asPath, '?'), 1)), [
    asPath,
  ]);
  return queryObject;
}
